Main components:
- Dockerfile (we dockerize the app code to optimize docker image layers and size to improve performance with deploys)
- .gitlab-ci.yaml (GitLab build pipeline, will compile, test and build for you a new docker images with your code ready for deploy)
- src (Your app code updates should be on this folder) - Updated the file main.go to listen on an specific port 5000 for go-fiber.
- reports (Used for the build pipeline to report the test of your code)
- kompose (Here you will find a docker-compose file to test your images with the backend, also there are YAML deployments for Kubernetes)
- templates (to store additionals gitlab pipelines if needed)

```
$/kompose/tree
|____frontend (Kube go-fiber app)
| |____j2m-api-deployment.yaml
| |____j2m-api-service.yaml
|____backend (Kube redis backend)
| |____redis-configmap.yaml
| |____redis-service.yaml
| |____redis-stafulset.yaml
| |____redis-volume-pv.yaml
|____docker-compose.yaml
|____.env  (.env with the environment variables on docker-compose)
|____redis.conf (redis.conf for redis contaniner setup on docker-compose)
```   

Build Pipeline.

Manual steps explained (this are automated with gitlab-ci)

1- Perform a linting of the code

$cd src/ ;golint ./.. 

2- Download package dependences with mod download

$cd src/ ; $go get -v or $go mod download

 Review go env dependeces downloaded
 $cd `go env GOPATH` && cd pkg/mod/github.com
 or
 $ls -lrtd `go env GOPATH`/pkg/mod/github.com/*

3- Compile the src code and create the binary file 
   Compile for linux ARCH amd64
$cd src/
$CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o j2m-lnx64

NOTE: This binary file only will work on linux machine, if you test it on MAC os will not work.

4- Test binary file file 

$./j2m
```
 ┌───────────────────────────────────────────────────┐
 │                   Fiber v2.15.0                   │
 │               http://127.0.0.1:5000               │
 │       (bound on host 0.0.0.0 and port 5000)       │
 │                                                   │
 │ Handlers ............. 2  Processes ........... 1 │
 │ Prefork ....... Disabled  PID ........... 1015885 │
 └───────────────────────────────────────────────────┘

```

- Perform some test (linting, format, cover)

$go test -v
$go test -coverprofile=cover.txt
$go tool cover -html=cover.txt -o cover.html


5- Build the Docker image file (last stage of gitlab pipeline build:image)

$docker build -f <your_dockerhub_user>/j2m-lnx64:v1 .


## Docker-compose test ##
Follow these step to test your docker image generated with the gitlab pipeline with docker-compose

Require install locally:
-docker => 18
-docker-compose

```
$cd kompose

Verifiy that you are using the latest docker image from your gitlab container repo
$cat .env
# environment file for docker-compose
REPO=registry.gitlab.com/victorbecerra
IMAGE=test-ci-go
TAG=latest
...

Run docker-compose:
$docker-compose up 

Verify from browser opening the link : http://127.0.0.1:5000

```
## Kubernetes deployment test ##
Follow these step to test your docker image generated with the gitlab pipeline with kubernetes
Require install locally:
- minikube or kind
- kubectl
- Create a minikube cluster 

```
$cd kompose

#Create a namespace "dev" for test and setup the cluster context
$kubectl create namespace dev
$kubectl config set-context --current --namespace dev

#Deploy the code, first the backend and next the frontend
$kubectl apply -f backend/
$kubectl apply -f frontend/

#Take note of the IP address of the node where is running j2m-api.
# For minikube 

$minikube ip
$IP=`minikube ip`

#Take the nodeport exposes for j2m-api services
$kubectl get service j2m-api -o jsonpath="{.spec.ports[0].nodePort}"
PORT=`kubectl get service j2m-api -o jsonpath="{.spec.ports[0].nodePort}"`

echo http://$IP:$PORT

```
