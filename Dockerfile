FROM golang:1.16-alpine AS builder

LABEL maintainer="SRE team <sre@acme.dev>"

# Work directory
WORKDIR /build

# Copy and download dependencies using go

COPY src/go.mod src/go.sum kompose/.env ./
RUN go mod download

# Copy go code to the container
COPY src/main.go src/main_test.go ./

# Set env variables for image and build the app
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
RUN go build -ldflags="-s -w" -o j2m-api .

FROM scratch

# Copy binary and test files from /build to root container.
COPY --from=builder ["/build/j2m-api","/build/.env","/"]

# Expose port 5000
EXPOSE 5000
# Command to run when starting the container
CMD ["/j2m-api"]
