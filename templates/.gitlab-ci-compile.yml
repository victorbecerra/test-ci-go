---
# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Go.gitlab-ci.yml

image: golang:1.16

variables:
  # Please edit to your GitLab project
  REPO_NAME: gitlab.com/namespace/project

# The problem is that to be able to use go get, one needs to put
# the repository in the $GOPATH. So for example if your gitlab domain
# is gitlab.com, and that your repository is namespace/project, and
# the default GOPATH being /go, then you'd need to have your
# repository in /go/src/gitlab.com/namespace/project
# Thus, making a symbolic link corrects this.

before_script:
  - mkdir -p $GOPATH/src/$(dirname $REPO_NAME)
  - ln -svf $CI_PROJECT_DIR $GOPATH/src/$REPO_NAME
  - cd $GOPATH/src/$REPO_NAME

stages:
  - linting
  - test
  - publish
  - build_app
  - deploy 

go-lint:
  stage: linting
  image: registry.gitlab.com/pipeline-components/go-lint:latest
  script:
    - golint ./...

format:
  stage: test
  script:
    - go fmt $(go list ./... | grep -v /vendor/)
    - go vet $(go list ./... | grep -v /vendor/)
    - go test -race $(go list ./... | grep -v /vendor/)

test:cover:
  stage: test
  script:
    - go test -coverprofile=cover.txt
  artifacts:
    expire_in: 1 week
    paths:
      - cover.txt

publish:cover:
  stage: publish
  dependencies:
    - test:cover
  script:
    - go tool cover -html=cover.txt -o $CI_PROJECT_DIR/reports/cover.html

compile:
  stage: build_app
  image: golang:1.16
  script:
    - go build -race -ldflags "-extldflags '-static'" -o $CI_PROJECT_DIR/bin
  rules:
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME'
  artifacts:
    paths:
      - bin

build:image:
  image: docker:stable
  services:
    - docker:dind

  variables:
    DOCKER_HOST: tcp://docker:2375
    DOCKER_DRIVER: overlay2

  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.

  script:
    - docker build --pull -t $CONTAINER_TEST_IMAGE .
    - docker push $CONTAINER_TEST_IMAGE
